﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : AbstractEnemies {
	private int[] random = {-1,1};
	private void Start() {
	enemyDirection = new Vector3(enemySpeed * random[Random.Range(0, random.Length)], 0, 0);
	enemyOriginalHealth = enemyHP;
	enemyRigidBody2D = GetComponent<Rigidbody2D>();
	}
	private void Update() {
		if(enemyDirection.x < 0)
		{
			enemySpriteRenderer.flipX= true;
		}
		else
			enemySpriteRenderer.flipX = false;
	}
	protected override void MovementBehaviour()
	{
		enemyRigidBody2D.velocity = enemyDirection;
	}
	private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "RightWall")
        {
			
			enemyDirection.x = -enemyDirection.x;

        }
        if (other.gameObject.tag == "LeftWall")
        {
			enemyDirection.x = -1*enemyDirection.x; 
			
        }
		
	}
	private void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag == "FirePit")
		{
			this.gameObject.SetActive(false);
			Instantiate(enemyBlood, transform.position, Quaternion.identity);
			GameObject redSlime = RedSlimePooler.SharedInstance.Pool();
			if(GameManager.SharedInstance.isPlaying && redSlime != null)
			{
				redSlime.transform.position = GameManager.SharedInstance.spawnPoint[Random.Range(0, GameManager.SharedInstance.spawnPoint.Length)].transform.position;
				redSlime.SetActive(true);
			}		
		}
		if(other.gameObject.tag == "ShotGunBullet")
		{
			enemyHP -= 3;
			Instantiate(enemyBlood, transform.position, Quaternion.identity);
			if(enemyHP <= 0)
			{
				enemyHP = enemyOriginalHealth;
				this.gameObject.SetActive(false);
			}
		}	
		if(other.gameObject.tag == "MachineGunBullet")
		{
			enemyHP -= 1;
			Instantiate(enemyBlood, transform.position, Quaternion.identity);
			if(enemyHP <= 0)
			{
				enemyHP = enemyOriginalHealth;
				this.gameObject.SetActive(false);
			}
		}
		if(other.gameObject.tag == "PistolBullet")
		{
			enemyHP -= 2;
			Instantiate(enemyBlood, transform.position, Quaternion.identity);
			if(enemyHP <= 0)
			{
				enemyHP = enemyOriginalHealth;
				this.gameObject.SetActive(false);
			}	
		}
	}
	
	
}
