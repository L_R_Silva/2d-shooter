﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingThing : AbstractEnemies {

	[SerializeField] private float frequency;
	[SerializeField][Range(0,1)] private float magnitude;
	private bool isFacingRight = false;
	private Vector3 flyingStuffPos;
	private Vector3 flyingStuffLocalScale;

	private void Start() {
		flyingStuffPos = transform.position;
		flyingStuffLocalScale = transform.localScale;
		enemyOriginalHealth = enemyHP;		
	}

	private void Update() {
		
		MovementBehaviour();

	}
	private void CheckWhereToFace()
	{
		if(flyingStuffPos.x < 0f)
		{
			isFacingRight = true;
		}
		else if(flyingStuffPos.x > 248f)
			isFacingRight = false;
		if(((isFacingRight) && (flyingStuffLocalScale.x < 0)) || ((!isFacingRight) && (flyingStuffLocalScale.x > 0)))
		{
			flyingStuffLocalScale *= -1;
		}
		transform.localScale = flyingStuffLocalScale;
	}
	private void MoveRight()
	{
		flyingStuffPos += transform.right * Time.deltaTime * enemySpeed;
		transform.position = flyingStuffPos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude;
	}
	private void MoveLeft()
	{
		flyingStuffPos -= transform.right * Time.deltaTime * enemySpeed;
		transform.position = flyingStuffPos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude;
	}
	protected override void MovementBehaviour()
	{
		CheckWhereToFace();

		if(isFacingRight)
		{
			MoveRight();
		}
		else
		{
			MoveLeft();
		}
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag == "ShotGunBullet")
		{
			enemyHP -= 3;
			Instantiate(enemyBlood, transform.position, Quaternion.identity);
			if(enemyHP <= 0)
			{
				enemyHP = enemyOriginalHealth;
				this.transform.position = GameManager.SharedInstance.flyingSpawnPoint[Random.Range(0, GameManager.SharedInstance.flyingSpawnPoint.Length)].transform.position;
				this.gameObject.SetActive(false);
			}
		}	
		if(other.gameObject.tag == "MachineGunBullet")
		{
			enemyHP -= 1;
			Instantiate(enemyBlood, transform.position, Quaternion.identity);
			if(enemyHP <= 0)
			{
				enemyHP = enemyOriginalHealth;
				this.transform.position = GameManager.SharedInstance.flyingSpawnPoint[Random.Range(0, GameManager.SharedInstance.flyingSpawnPoint.Length)].transform.position;
				this.gameObject.SetActive(false);
			}
		}
		if(other.gameObject.tag == "PistolBullet")
		{
			enemyHP -= 2;
			Instantiate(enemyBlood, transform.position, Quaternion.identity);
			if(enemyHP <= 0)
			{
				enemyHP = enemyOriginalHealth;
				this.transform.position = GameManager.SharedInstance.flyingSpawnPoint[Random.Range(0, GameManager.SharedInstance.flyingSpawnPoint.Length)].transform.position;
				this.gameObject.SetActive(false);
			}	
		}
		
	
	}

}
