﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
	[SerializeField] private Transform playerCharacter;
	private Vector3 posCamera;
	private void Start()
	 {
		if(playerCharacter != null)
			CameraMoviment();
		else
		{
			playerCharacter = FindPlayer();			
		}
	}
	private void LateUpdate() 
	{
		if(playerCharacter != null)
			CameraMoviment();
		else
		{
			playerCharacter = FindPlayer();			
		}
	}

	private void CameraMoviment()
	{
		
		posCamera.x = this.transform.position.x;
		if(playerCharacter.transform.position.y >= -174f && playerCharacter.transform.position.y <= -83.285f)
			posCamera.y = playerCharacter.transform.position.y;
		else if(playerCharacter.transform.position.y <= -174f)
			posCamera.y = -173;
		else if(playerCharacter.transform.position.y <= -83.285f)
			posCamera.y = -83.285f;
		posCamera.z = this.transform.position.z;
		this.transform.position = posCamera;
	}

	private Transform FindPlayer()
	{
		Transform _playerCharacter = (GameObject.FindGameObjectWithTag("Player") ? GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>() : null);
		return _playerCharacter;
	}

}
