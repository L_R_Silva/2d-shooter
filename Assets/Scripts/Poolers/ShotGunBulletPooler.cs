﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotGunBulletPooler : AbstractPooler {
	
	public static ShotGunBulletPooler SharedInstance;
	private void Awake() 
	{
		SharedInstance = this;
	}
}
