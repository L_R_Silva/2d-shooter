﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : PhysicsObjects
{

    [SerializeField]
    private float jumpSpeed = 7;
    [SerializeField]
    private float maxSpeed = 7;
    private SpriteRenderer spr;
    //Animator
    private Animator anim;
    [SerializeField]private GameObject playerBlood;
    void Awake()
    {
        //Get Reference
        spr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && grounded)
        {
            velocity.y = jumpSpeed;
        }
        else if (Input.GetButtonUp("Jump"))
            if (velocity.y > 0)
                velocity.y *= .5f;

        bool _flipSpr = (spr.flipX ? (move.x > 0.01f) : (move.x < 0.01f));
        if (_flipSpr)
        {
            spr.flipX = !spr.flipX; // flip x spr 
        }

        anim.SetBool("grounded", grounded);
        anim.SetFloat("Velocity.x", Mathf.Abs(velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Instantiate(playerBlood, transform.position, Quaternion.identity);
            this.gameObject.SetActive(false);
            GameManager.SharedInstance.isPlaying = false;
            GameObject gun = GameObject.FindGameObjectWithTag("Weapons");
            Destroy(gun);            
            
        }
        

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.tag == "FirePit")
        {
            Instantiate(playerBlood, transform.position, Quaternion.identity);
            this.gameObject.SetActive(false); 
            GameManager.SharedInstance.isPlaying = false;
            GameObject gun = GameObject.FindGameObjectWithTag("Weapons");
		    Destroy(gun);           
            
        }
        if (collision.gameObject.tag == "Enemy")
        {
            Instantiate(playerBlood, transform.position, Quaternion.identity);
            this.gameObject.SetActive(false);
            GameManager.SharedInstance.isPlaying = false;
            GameObject gun = GameObject.FindGameObjectWithTag("Weapons");
			Destroy(gun);            
            
        }
    }
}
