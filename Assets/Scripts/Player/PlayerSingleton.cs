﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSingleton : GenericSingleton<PlayerSingleton> {

	protected PlayerSingleton() { }
}
