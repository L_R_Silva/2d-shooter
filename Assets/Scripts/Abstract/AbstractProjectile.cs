﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public abstract class AbstractProjectile : MonoBehaviour {

	[SerializeField] protected float projectileSpeed;
	[SerializeField][Range (1,3)]private int projectileDamage;
	[SerializeField][Range (0,3)]protected float projectileLifeTime;
	protected float projectileOriginalLifeTime;
	protected Rigidbody2D projetileRigidBody2D;
	[HideInInspector]public Vector3 projectileDirection;
	protected Vector3 projectilePos;
	protected bool isActive = false;
	private void Start() 
	{
		projetileRigidBody2D = GetComponent<Rigidbody2D>();
		projectileOriginalLifeTime = projectileLifeTime;
	}
	private void FixedUpdate() 
	{
		MovementPattern();	
	}
	private void Update() 
	{
		projectileLifeTime -= Time.deltaTime;
		if(projectileLifeTime < 0)
		{
			SetProjectileToFalse(gameObject);
		}
	}


	protected abstract void MovementPattern();
	protected abstract void SetProjectileToFalse(GameObject _projectile);


}
