﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractEnemies : MonoBehaviour {

	[SerializeField] protected float enemySpeed;
	[SerializeField][Range(1,5)]protected int enemyHP;
	[SerializeField] protected SpriteRenderer enemySpriteRenderer;
	protected Rigidbody2D enemyRigidBody2D;
	[SerializeField] protected GameObject enemyBlood;
	protected Vector3 enemyDirection;
	protected int enemyOriginalHealth;
	private void Awake() {
		IgnoreCollisionOnLayers();
	}
	private void FixedUpdate() {
		MovementBehaviour();
	}
	protected abstract void MovementBehaviour();
	
	private void IgnoreCollisionOnLayers()
	{
		Physics2D.IgnoreLayerCollision(10,10,true);
		Physics2D.IgnoreLayerCollision(10,11,true);
		Physics2D.IgnoreLayerCollision(10,12,true);
		Physics2D.IgnoreLayerCollision(11,12,true);
	}

	
}
