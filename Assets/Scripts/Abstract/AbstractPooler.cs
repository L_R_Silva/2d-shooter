﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractPooler : MonoBehaviour {

	[SerializeField] protected GameObject gameObjectToPool;
	[SerializeField][Range (0,100)] protected int numberOfObjects;
	protected List<GameObject> objectPool;

	private void Start() 
	{
		CreatePool();			
	}
	private void CreatePool()
	{
		objectPool = new List<GameObject>();
		for(int i = 0; i < numberOfObjects; i++)
		{
			GameObject _temp = (GameObject)Instantiate(gameObjectToPool);
			_temp.SetActive(false);
			objectPool.Add(_temp);
		}
	}
	public GameObject Pool()
	{
		for(int i = 0; i < objectPool.Count; i++)
		{
			if(!objectPool[i].activeInHierarchy)
				return objectPool[i];
		}
		return null;
	}
}
