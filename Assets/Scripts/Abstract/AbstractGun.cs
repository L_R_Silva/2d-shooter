﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractGun : MonoBehaviour {

	[SerializeField] protected float mouseSen = 3f;
	private Vector3 mousePos = Vector3.zero;
	[SerializeField] private Transform gun;
	[SerializeField] private Transform player;
	protected SpriteRenderer spriteRenderer;
	protected Color initColor;
	[SerializeField][Range(0,1)] protected float maxGunHot;
	[SerializeField]protected GameObject[] gunPoint;
	protected float lerpSpeed;
	protected float fireTimer;
	protected float coldTimer;
	[SerializeField] protected float timeBetweenShoots; 
	protected bool canFire = true;
	protected float timer = 0f;

	private void Awake() 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
        initColor = Color.white;
		player = FindPlayer();	

	}
	protected void GunOrbit()
	{
		//Rotate pivot point
        mousePos = Input.mousePosition;
        mousePos.z = 0f;
        Vector3 objPos = Camera.main.WorldToScreenPoint(gun.position);
        mousePos.x = mousePos.x - objPos.x;
        mousePos.y = mousePos.y - objPos.y;
        float _angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
        Vector3 _rotation = new Vector3(0, 0, _angle);
		transform.rotation = Quaternion.Euler(_rotation);
		bool _gunFlipY = (spriteRenderer.flipY ?  (_angle >-90f && _angle < 90f) : (_angle > 90f));
		if(_gunFlipY)
		{
			spriteRenderer.flipY = !spriteRenderer.flipY;
		}
        //Rotaciona objeto em torno de outro objeto(PLayer)
        Vector3 _targetScreenPos = Camera.main.WorldToScreenPoint(player.position);
        _targetScreenPos.z = 0;
        Vector3 _mouseDir = Input.mousePosition - _targetScreenPos;
        Vector3 _targetToMe = transform.position - player.position;
        _targetToMe.z = 0;
        Vector3 _newTargetToMe = Vector3.RotateTowards(_targetToMe, _mouseDir, mouseSen/*Angle por frame*/, 0f);
        transform.position = player.position + 10f/*Distancia do Centro*/ * _newTargetToMe.normalized;
	}
	private Transform FindPlayer()
	{
		Transform _playerCharacter = (GameObject.FindGameObjectWithTag("Player") ? GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>() : null);
		return _playerCharacter;
	}
	protected abstract void Shoot();
	private void FixedUpdate() {
		
		timer += Time.deltaTime;
		if(timer >= timeBetweenShoots)
		{
			Shoot();
			timer = 0;
		}
		GunOrbit();
	}
}
