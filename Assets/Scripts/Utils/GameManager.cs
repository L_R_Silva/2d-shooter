﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI score;   
    public GameObject[] spawnPoint;
    public GameObject[] flyingSpawnPoint;
    [SerializeField] private GameObject[] spawnCratePoint;
    [SerializeField] private float SpawnRateEnemies;
    [SerializeField] private float SpawnRateCrates;
    [SerializeField] private int cratesToWin;
    [SerializeField] private GameObject WinPanel;
    [SerializeField] private GameObject LosePanel;
    public bool isPlaying = true;
    private bool won = false;
    public static GameManager SharedInstance;
    private Queue<GameObject> spawnQueue = new Queue<GameObject>();
    private Queue<GameObject> flyingSpawnQueue = new Queue<GameObject>();
    private Queue<GameObject> spawnCrateQueue = new Queue<GameObject>();
    private void Start() {
        SharedInstance = this;
        for(int i = 0; i < spawnPoint.Length; i++)
        {
            spawnQueue.Enqueue(spawnPoint[i]);   
        }
        for(int i = 0; i < flyingSpawnPoint.Length; i++)
        {
            flyingSpawnQueue.Enqueue(flyingSpawnPoint[i]);   
        }
        for(int i = 0; i < spawnCratePoint.Length; i++)
        {
            spawnCrateQueue.Enqueue(spawnCratePoint[i]);   
        }
        StartCoroutine("SpawnEnemies");
        StartCoroutine("SpawnFlyingEnemies");
        StartCoroutine("SpawnCrates");
    }

    private IEnumerator SpawnEnemies()
    {
        bool smallNow = true;
        while(isPlaying)
        {
            yield return new WaitForSeconds(SpawnRateEnemies);
            GameObject SpawnPoint = spawnQueue.Dequeue();
            GameObject big = SlimePooler.SharedInstance.Pool();
            if(big != null && SpawnPoint != null && smallNow == false)
            {
                big.transform.position = SpawnPoint.transform.position;
                big.SetActive(true);
                smallNow = true;
            }
            else
            {
                GameObject small = SmallSlimePooler.SharedInstance.Pool();
                if(small != null && SpawnPoint != null)
                {
                    small.transform.position = SpawnPoint.transform.position;
                    small.SetActive(true);
                    smallNow = false;
                }
                else 
                    break;
            }
            spawnQueue.Enqueue(SpawnPoint);
        }
    }
   
    private IEnumerator SpawnFlyingEnemies()
    {
        while(isPlaying)
        {
            yield return new WaitForSeconds(SpawnRateEnemies);
            GameObject SpawnPoint = flyingSpawnQueue.Dequeue();
            GameObject flyingStuff = FlyingStuffPooler.SharedInstance.Pool();
            if(flyingStuff != null && SpawnPoint != null)
            {
                flyingStuff.SetActive(true);
                flyingStuff.transform.position = SpawnPoint.transform.position;
            }
            flyingSpawnQueue.Enqueue(SpawnPoint);
        }
    }

    private IEnumerator SpawnCrates()
    {
        while(isPlaying)
        {
            yield return new WaitForSeconds(SpawnRateCrates);
            GameObject SpawnPoint = spawnCrateQueue.Dequeue();
            GameObject crate = CratePooler.SharedInstance.Pool();
            if(crate != null && SpawnPoint != null)
            {
                crate.transform.position = SpawnPoint.transform.position;
                crate.SetActive(true);
            }
            spawnCrateQueue.Enqueue(SpawnPoint);
        }
    }
     private void Update() {
        if(!isPlaying)
        {
            StopCoroutine("SpawnEnemies");
            StopCoroutine("SpawnFlyingEnemies");
            StopCoroutine("SpawnCrates");
            StartCoroutine("Lose");
        } 
        if(won)
        {
            StartCoroutine("Win");
          
        } 
        if(Input.GetKeyDown(KeyCode.F5))
        {
            won = true;
        }  
    }
    private IEnumerator Win()
    {
        yield return new WaitForSeconds(1);
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        if (nextSceneIndex < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(nextSceneIndex);
            
        }
        else
            Application.Quit();
    }

    private IEnumerator Lose()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void SetScore(int points)
    {
        
        score.text = (Int32.Parse(score.text) + points).ToString();
        if(Int32.Parse(score.text) >= cratesToWin)
        {
            won = true;
            
        }
    }

 }
