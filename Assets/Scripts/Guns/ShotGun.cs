﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotGun : AbstractGun {

	private void Start() {
		lerpSpeed = 4f;
	}
	protected override void Shoot()
	{
		
		if(Input.GetAxisRaw("Fire1") == 1 && canFire == true)
		{
			fireTimer += Time.deltaTime;
			spriteRenderer.color = Color.Lerp(initColor, Color.red, Mathf.Lerp(0, fireTimer * lerpSpeed, 0.05f));
			initColor = spriteRenderer.color;
			List<GameObject> _bullet = new List<GameObject>();
			for(int i=0; i < 4; i++)
			{
				_bullet.Add(ShotGunBulletPooler.SharedInstance.Pool());
			}
			Vector3 _direction = Input.mousePosition;
			for(int i = 0; i < _bullet.Count; i ++)
			{
				if(_bullet[i] != null)
				{
					//_bullet[i].projectileDirection = (Input.mousePostion)
					_bullet[i].transform.position = gunPoint[Random.Range(0, gunPoint.Length)].transform.position;
					_bullet[i].GetComponent<ShotGunProjectile>().SetDirection(_direction); 
					_bullet[i].SetActive(true);
					CameraShake.SharedInstance.ShakeCamera(3f, .1f);
					
				}
			
				
			}
		}
		else
		{
			fireTimer -= Time.deltaTime;
			coldTimer += Time.deltaTime;
			spriteRenderer.color = Color.Lerp(initColor, Color.white, Mathf.Lerp(0, coldTimer * lerpSpeed, 1f));
			initColor =spriteRenderer.color;
			if (fireTimer <= 0)
			{
				canFire = true;
				fireTimer = 0;
			}
			if (coldTimer <= 0 || coldTimer <= maxGunHot)
			{
				coldTimer = 0;
			}
		
		}
		if (fireTimer >= maxGunHot)
			canFire = false;
	}
}
