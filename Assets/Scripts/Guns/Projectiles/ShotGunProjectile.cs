﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotGunProjectile : AbstractProjectile {

    private int hitCount = 0;
    protected override void MovementPattern()
    {
        if (isActive == false)
        {
             projetileRigidBody2D.velocity = projectileDirection * projectileSpeed;
            isActive = true;
        }
    }
    protected override void SetProjectileToFalse(GameObject _projectile)
    {
        _projectile.SetActive(false);
        isActive = false;
        projectileLifeTime = projectileOriginalLifeTime;
    }

    public void SetDirection(Vector3 _direction)
    {
        projectilePos = Camera.main.WorldToScreenPoint(transform.position);
        projectileDirection = (_direction - projectilePos).normalized;

    }

    	private void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.gameObject.tag == "Enemy") 
		{
            hitCount +=1;
            if(hitCount >= 3)
            { 
			    hitCount = 0;
                SetProjectileToFalse(gameObject);
            }
        }
	}	
}
