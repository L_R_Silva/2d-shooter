﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolProjectile : AbstractProjectile {

	protected override void MovementPattern()
    {
        if (isActive == false)
        {
            projectilePos = Camera.main.WorldToScreenPoint(transform.position);
            projectileDirection = (Input.mousePosition - projectilePos).normalized;
            projetileRigidBody2D.velocity = projectileDirection * projectileSpeed;
            isActive = true;
        }
    }
    protected override void SetProjectileToFalse(GameObject _projectile)
    {
        _projectile.SetActive(false);
        isActive = false;
        projectileLifeTime = projectileOriginalLifeTime;
    }

	private void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.gameObject.tag == "Enemy") 
		{
			SetProjectileToFalse(gameObject);
		}
	}	
}
