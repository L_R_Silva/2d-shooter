﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGun : AbstractGun {
	private void Start() {
		lerpSpeed = 4f;
	}
	protected override void Shoot()
	{
		if(Input.GetAxisRaw("Fire1") == 1 && canFire == true)
		{
			fireTimer += Time.deltaTime;
			spriteRenderer.color = Color.Lerp(initColor, Color.red, Mathf.Lerp(0, fireTimer * lerpSpeed, 0.05f));
			initColor = spriteRenderer.color;
			GameObject _bullet = MachineGunBulletPooler.SharedInstance.Pool();
			if(_bullet != null)
			{
				_bullet.transform.position = gunPoint[Random.Range(0, gunPoint.Length)].transform.position;
				_bullet.SetActive(true);
				CameraShake.SharedInstance.ShakeCamera(1.5f, .1f);
			}
		}
		else
		{
			fireTimer -= Time.deltaTime;
			coldTimer += Time.deltaTime;
			spriteRenderer.color = Color.Lerp(initColor, Color.white, Mathf.Lerp(0, coldTimer * lerpSpeed, 1f));
			initColor =spriteRenderer.color;
			if (fireTimer <= 0)
			{
				canFire = true;
				fireTimer = 0;
			}
			if (coldTimer <= 0 || coldTimer <= maxGunHot)
			{
				coldTimer = 0;
			}
		
		}
		if (fireTimer >= maxGunHot)
			canFire = false;
	}
	
	

}

