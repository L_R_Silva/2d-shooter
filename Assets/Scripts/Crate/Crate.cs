﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crate : MonoBehaviour {

	[SerializeField] private GameObject[] Weapons;
	[SerializeField] private int points;

	private void OnCollisionEnter2D(Collision2D other) {
		
		if(other.gameObject.tag == "Player")
		{
			GameObject gun = GameObject.FindGameObjectWithTag("Weapons");
			Destroy(gun);
			Instantiate(Weapons[Random.Range(0,Weapons.Length)],transform.position, Quaternion.identity);
			GameManager.SharedInstance.SetScore(points);
			this.gameObject.SetActive(false);
		}
		if(other.gameObject.tag == "Box")
		{
			this.gameObject.SetActive(false);
		}

	}
}
